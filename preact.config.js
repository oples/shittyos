const tailwindCSS = require('tailwindcss');

module.exports = (config, env, helpers) => {
  const postCssLoaders = helpers.getLoadersByName(config, 'postcss-loader');
  postCssLoaders.forEach(({ loader }) => {
    const plugins = loader.options.postcssOptions.plugins;

    // Add tailwindcss to top of plugins list
    plugins.unshift(tailwindCSS);
  });

  return config;
};
